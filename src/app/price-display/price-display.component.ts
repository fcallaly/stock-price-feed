import { Component, OnInit } from '@angular/core';
import { PriceFeedService } from '../price-feed.service';

@Component({
  selector: 'app-price-display',
  templateUrl: './price-display.component.html',
  styleUrls: ['./price-display.component.css']
})
export class PriceDisplayComponent implements OnInit {

  public ticker = "C";
  public numDays = 1;
  public priceData: any;

  constructor(private priceFeedService: PriceFeedService) { }

  ngOnInit(): void { }

  getPriceData() {
    this.priceFeedService.getPrice(this.ticker, this.numDays).subscribe(
      response => {
        console.log("retrieved price data:")
        console.log(response);
        this.priceData = response.price_data;
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    )
  }
}
